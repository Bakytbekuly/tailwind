import { NavTypesType } from "@/types/config";

export const navTypes: NavTypesType[] = [
  {
    label: "Sticky",
    value: "sticky",
  },
  {
    label: "Static",
    value: "static",
  },
  {
    label: "Floating",
    value: "floating",
  },
  {
    label: "Hidden",
    value: "hidden",
  },
];
