import { FooterType } from "@/types/config";

export const footerTypes: FooterType[] = [
  {
    label: "Sticky",
    value: "sticky",
  },
  {
    label: "Static",
    value: "static",
  },

  {
    label: "Hidden",
    value: "hidden",
  },
];
