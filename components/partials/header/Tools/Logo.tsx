"use client";
import Link from "next/link";
import { useLayoutStore } from "@/store";
import useWidth from "@/hooks/useWidth";
import logoWhite from "../../../../public/assets/images/logo/logo-white.svg";
import logo from "../../../../public/assets/images/logo/logo.svg";
import Image from "next/image";

const Logo = () => {
  const { darkMode } = useLayoutStore();
  const { width, breakpoints } = useWidth();
  return (
    <div>
      <Link href="/">
        <>
          {Number(width) >= Number(breakpoints.xl) ? (
            <Image src={darkMode ? logoWhite : logo} alt="" />
          ) : (
            <Image src={darkMode ? logoWhite : logo} alt="" />
          )}
        </>
      </Link>
    </div>
  );
};

export default Logo;
