"use client";
import React from "react";
import Icon from "@/components/ui/Icon";
import { useLayoutStore } from "@/store";

const SwitchDark = () => {
  const { darkMode, setDarkMode } = useLayoutStore();
  return (
    <span>
      <div
        className="lg:h-[32px] lg:w-[32px] lg:bg-slate-100 lg:dark:bg-slate-900 dark:text-white text-slate-900 cursor-pointer rounded-full text-[20px] flex flex-col items-center justify-center"
        onClick={() => setDarkMode(!darkMode)}
      >
        {darkMode ? (
          <Icon icon="heroicons-outline:sun" />
        ) : (
          <Icon icon="heroicons-outline:moon" />
        )}
      </div>
    </span>
  );
};

export default SwitchDark;
