"use client";
import React from "react";
import Icon from "@/components/ui/Icon";
import useWidth from "@/hooks/useWidth";
import SwitchDark from "./Tools/SwitchDark";
import HorizentalMenu from "./Tools/HorizentalMenu";
import SearchModal from "./Tools/SearchModal";
import Profile from "./Tools/Profile";
import Notification from "./Tools/Notification";
import Message from "./Tools/Message";
import Language from "./Tools/Language";
import Logo from "./Tools/Logo";
import { useLayoutStore } from "@/store";

const Header = ({ className = "custom-class" }) => {
  const {
    isCollapsed,
    setIsCollapsed,
    navBarType,
    menuType,
    skin,
    isRtl,
    mobileMenu,
    setMobileMenu,
  } = useLayoutStore();
  const { width, breakpoints } = useWidth();
  const navbarTypeClass = () => {
    switch (navBarType) {
      case "floating":
        return "floating  has-sticky-header";
      case "sticky":
        return "sticky top-0 z-[999]";
      case "static":
        return "static";
      case "hidden":
        return "hidden";
      default:
        return "sticky top-0";
    }
  };

  const handleOpenMobileMenu = () => {
    setMobileMenu(!mobileMenu);
  };
  const borderSwitchClass = () => {
    if (skin === "bordered" && navBarType !== "floating") {
      return "border-b border-slate-200 dark:border-slate-700";
    } else if (skin === "bordered" && navBarType === "floating") {
      return "border border-slate-200 dark:border-slate-700";
    } else {
      return "dark:border-b dark:border-slate-700 dark:border-opacity-60";
    }
  };
  return (
    <header className={`${className} ${navbarTypeClass()}`}>
      <div
        className={`app-header md:px-6 px-[15px] dark:bg-slate-800 shadow-base dark:shadow-base3 bg-white
                    ${borderSwitchClass()}
                    ${
                      menuType === "horizontal" &&
                      Number(width) > Number(Number(breakpoints.xl))
                        ? "py-1"
                        : "md:py-6 py-3"
                    }`}
      >
        <div className="flex justify-between items-center h-full">
          {/* For Vertical */}
          {menuType === "vertical" && (
            <div className="flex items-center md:space-x-4 space-x-2 rtl:space-x-reverse">
              {isCollapsed && Number(width) >= Number(breakpoints.xl) && (
                <button
                  className="text-xl text-slate-900 dark:text-white"
                  onClick={() => setIsCollapsed(!isCollapsed)}
                >
                  {isRtl ? (
                    <Icon icon="akar-icons:arrow-left" />
                  ) : (
                    <Icon icon="akar-icons:arrow-right" />
                  )}
                </button>
              )}
              {Number(width) < Number(breakpoints.xl) && <Logo />}
              {/* open mobile menu handler */}
              {Number(width) < Number(breakpoints.xl) &&
                Number(width) >= Number(breakpoints.md) && (
                  <div
                    className="cursor-pointer text-slate-900 dark:text-white text-2xl"
                    onClick={handleOpenMobileMenu}
                  >
                    <Icon icon="heroicons-outline:menu-alt-3" />
                  </div>
                )}
              <SearchModal />
            </div>
          )}
          {/* For Horizontal */}
          {menuType === "horizontal" && (
            <div className="flex items-center space-x-4 rtl:space-x-reverse">
              <Logo />
              {/* open mobile menu handler */}
              {Number(width) <= Number(breakpoints.xl) && (
                <div
                  className="cursor-pointer text-slate-900 dark:text-white text-2xl"
                  onClick={handleOpenMobileMenu}
                >
                  <Icon icon="heroicons-outline:menu-alt-3" />
                </div>
              )}
            </div>
          )}
          {/* Horizontal Main Menu */}
          {menuType === "horizontal" && width >= Number(breakpoints.xl) ? (
            <HorizentalMenu />
          ) : null}
          {/* Nav Tools */}
          <div className="nav-tools flex items-center lg:space-x-6 space-x-3 rtl:space-x-reverse">
            <Language />
            <SwitchDark />

            {Number(width) >= Number(breakpoints.md) && <Message />}
            {Number(width) >= Number(breakpoints.md) && <Notification />}
            {Number(width) >= Number(breakpoints.md) && <Profile />}
            {Number(width) <= Number(breakpoints.md) && (
              <div
                className="cursor-pointer text-slate-900 dark:text-white text-2xl"
                onClick={handleOpenMobileMenu}
              >
                <Icon icon="heroicons-outline:menu-alt-3" />
              </div>
            )}
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
