"use client";
import { useAuthStore } from "@/store";
import { redirect, useRouter } from "next/navigation";

const LoginPage = () => {
  const { getUser } = useAuthStore();
  const router = useRouter();

  const handleLogin = () => {
    getUser().then(() => {
      router.push("/");
    });
  };

  return (
    <div>
      <h1>Login Page</h1>
      <button onClick={handleLogin}>Login</button>
    </div>
  );
};

export default LoginPage;
