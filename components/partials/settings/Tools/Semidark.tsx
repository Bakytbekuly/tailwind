import React from "react";
import { useLayoutStore } from "@/store";
import Switch from "@/components/ui/Switch";

const Semidark = () => {
  const { semiDarkMode, setSemiDarkMode } = useLayoutStore();
  return (
    <div className=" flex justify-between">
      <div className="text-slate-600 text-base dark:text-slate-300 font-normal">
        Semi Dark
      </div>
      <Switch
        value={semiDarkMode}
        onChange={() => setSemiDarkMode(!semiDarkMode)}
        id="semi_nav_tools"
      />
    </div>
  );
};

export default Semidark;
