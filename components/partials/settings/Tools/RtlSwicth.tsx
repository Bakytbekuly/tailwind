import React from "react";
import Switch from "@/components/ui/Switch";
import { useLayoutStore } from "@/store";

const RtlSwicth = () => {
  const { isRtl, setIsRtl } = useLayoutStore();
  return (
    <div className="flex justify-between">
      <div className="text-slate-600 text-base dark:text-slate-300">Rtl</div>
      <Switch
        value={isRtl}
        onChange={() => setIsRtl(!isRtl)}
        id="rtl_nav_tools"
      />
    </div>
  );
};

export default RtlSwicth;
