import React from "react";
import Radio from "@/components/ui/Radio";
import { useLayoutStore } from "@/store";
import { navTypes } from "@/constant/navBar";
import { ThemeConfigType } from "@/types/config";

const NavType = () => {
  const { navBarType, setNavBarType } = useLayoutStore();
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNavBarType(e.target.value as ThemeConfigType["layout"]["navBarType"]);
  };

  return (
    <div>
      <h4 className="text-slate-600 text-base dark:text-slate-300 mb-2 font-normal">
        Navbar Type
      </h4>
      <div className="grid md:grid-cols-4 grid-cols-1 gap-3">
        {navTypes?.map((item, index) => (
          <Radio
            key={index}
            label={item.label}
            name="navbarType"
            value={item.value}
            checked={navBarType === item.value}
            onChange={handleChange}
            className="h-4 w-4"
          />
        ))}
      </div>
    </div>
  );
};

export default NavType;
