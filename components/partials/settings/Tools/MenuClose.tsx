import React from "react";
import Switch from "@/components/ui/Switch";
import { useLayoutStore } from "@/store";

const MenuClose = () => {
  const { isCollapsed, setIsCollapsed } = useLayoutStore();
  return (
    <div className=" flex justify-between">
      <div className="text-slate-600 text-base dark:text-slate-300 font-normal">
        Menu Collapsed
      </div>
      <Switch
        value={isCollapsed}
        onChange={() => setIsCollapsed(!isCollapsed)}
        id="semi_nav_tools2"
      />
    </div>
  );
};

export default MenuClose;
