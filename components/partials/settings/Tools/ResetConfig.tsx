import React from "react";
import { useLayoutStore } from "@/store";
import Button from "@/components/ui/Button";
import Icons from "@/components/ui/Icon";

const ResetConfig = () => {
  const { setResetConfigs } = useLayoutStore();
  return (
    <div className="flex justify-between">
      <div className="text-slate-600 text-base dark:text-slate-300">
        Reset config
      </div>
      <div
        onClick={() => {
          setResetConfigs();
        }}
        className="cursor-pointer"
      >
        <Icons icon="heroicons-outline:arrow-path" />
      </div>
    </div>
  );
};

export default ResetConfig;
