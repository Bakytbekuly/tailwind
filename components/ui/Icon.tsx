"use client";
import React from "react";
import { Icon, IconifyIcon } from "@iconify/react";
type Props = {
  icon: string | IconifyIcon;
  className?: string;
  width?: string;
  rotate?: number;
  hFlip?: boolean;
  vFlip?: boolean;
};
const Icons = ({ icon, className, width, rotate, hFlip, vFlip }: Props) => {
  return (
    <>
      <Icon
        width={width}
        rotate={rotate}
        hFlip={hFlip}
        icon={icon}
        className={className}
        vFlip={vFlip}
      />
    </>
  );
};

export default Icons;
