import { User } from "@/types/auth";

export const getUser = async (): Promise<[User]> => {
  const response = await fetch("/user");

  if (!response.ok) throw new Error("Unable to fetch posts.");

  return response.json();
};
