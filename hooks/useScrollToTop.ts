import { useEffect, useRef, useState } from "react";

export default function useScrollToTop() {
  const scrollableNodeRef: any = useRef();
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    const handleScroll = () => {
      if (scrollableNodeRef.current.scrollTop > 0) {
        setScroll(true);
      } else {
        setScroll(false);
      }
    };
    scrollableNodeRef.current.addEventListener("scroll", handleScroll);
  }, [scrollableNodeRef]);

  return [scrollableNodeRef, scroll];
}
