declare namespace NodeJS {
  export interface ProcessEnv {
    readonly MY_ENV: string;
    readonly NEXT_PUBLIC_ENV_TEXT: string;
    readonly NEXT_PUBLIC_ENV_BASE_URL: string;
  }
}
