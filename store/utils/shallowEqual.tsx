"use client";
import { useLayoutStore as useLayoutStoreZus } from "@/store/store/useLayoutStore";
import { useAuthStore as useAuthStoreZus } from "@/store/store/useAuthStore";
import { useShallow } from "zustand/react/shallow";

function useLayoutStore() {
  return useLayoutStoreZus(useShallow((state) => state));
}

function useAuthStore() {
  return useAuthStoreZus(useShallow((state) => state));
}

export { useLayoutStore, useAuthStore };
