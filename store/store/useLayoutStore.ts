import { SettingsStoreType } from "@/types/settings";
import { create } from "zustand";
import { persist, devtools } from "zustand/middleware";
import { immer } from "zustand/middleware/immer";
// theme config import
import themeConfig, { NAME_TEXT } from "@/configs/themeConfig";

export const useLayoutStore = create<SettingsStoreType>()(
  immer(
    devtools(
      persist(
        (set) => ({
          menuType: themeConfig.layout.type,
          darkMode: themeConfig.layout.darkMode,
          isCollapsed: false,
          navBarType: themeConfig.layout.navBarType,
          skin: themeConfig.layout.skin,
          isRtl: themeConfig.layout.isRTL,
          mobileMenu: themeConfig.layout.mobileMenu,
          customizer: themeConfig.layout.customizer,
          semiDarkMode: themeConfig.layout.semiDarkMode,
          contentWidth: themeConfig.layout.contentWidth,
          menuHidden: themeConfig.layout.menu.isHidden,
          isMonochrome: themeConfig.layout.isMonochrome,
          footerType: themeConfig.layout.footerType,
          switchMenuType: (type) => {
            set(
              {
                menuType: type,
              },
              false,
              "switchMenuType",
            );
          },
          setDarkMode: (mode) => {
            set(
              {
                darkMode: mode,
              },
              false,
              "setDarkMode",
            );
          },
          setIsCollapsed: (isCollapsed) => {
            set(
              {
                isCollapsed,
              },
              false,
              "setIsCollapsed",
            );
          },
          setNavBarType: (navBarType) => {
            set({ navBarType }, false, "setNavBarType");
          },
          setSkin: (skin) => {
            set({ skin }, false, "setSkin");
          },
          setIsRtl: (isRtl) => {
            set({ isRtl }, false, "setIsRtl");
          },
          setMobileMenu: (mobileMenu) => {
            set({ mobileMenu }, false, "setMobileMenu");
          },
          setCustomizer: (customizer) => {
            set({ customizer }, false, "setCustomizer");
          },
          setSemiDarkMode: (semiDarkMode) => {
            set({ semiDarkMode }, false, "setSemiDarkMode");
          },
          setContentWidth: (contentWidth) => {
            set({ contentWidth }, false, "setContentWidth");
          },
          setMenuHidden: (menuHidden) => {
            set({ menuHidden }, false, "setMenuHidden");
          },
          setIsMonochrome: (isMonochrome) => {
            set({ isMonochrome }, false, "setIsMonochrome");
          },
          setFooterType: (footerType) => {
            set({ footerType }, false, "setFooterType");
          },
          setResetConfigs: () => {
            set({
              menuType: themeConfig.layout.type,
              darkMode: themeConfig.layout.darkMode,
              isCollapsed: false,
              navBarType: themeConfig.layout.navBarType,
              skin: themeConfig.layout.skin,
              isRtl: themeConfig.layout.isRTL,
              mobileMenu: themeConfig.layout.mobileMenu,
              customizer: themeConfig.layout.customizer,
              semiDarkMode: themeConfig.layout.semiDarkMode,
              contentWidth: themeConfig.layout.contentWidth,
              menuHidden: themeConfig.layout.menu.isHidden,
              isMonochrome: themeConfig.layout.isMonochrome,
              footerType: themeConfig.layout.footerType,
            });
          },
        }),
        NAME_TEXT,
      ),
      NAME_TEXT,
    ),
  ),
);
