import { create } from "zustand";
import { persist, devtools } from "zustand/middleware";
import { immer } from "zustand/middleware/immer";
import { UserStore } from "@/types/auth";
import { getUser } from "@/services/auth";

const NAME_TEXT = { name: "auth" };

export const useAuthStore = create<UserStore>()(
  immer(
    devtools(
      persist(
        (set) => ({
          user: null,
          getUser: async () => {
            try {
              const user = await getUser();

              set({
                user: user.length > 0 ? user[0] : null,
              });
            } catch (error) {
              console.log(error);
            }
          },
          logout: () => {
            set({
              user: null,
            });
          },
        }),
        NAME_TEXT,
      ),
      NAME_TEXT,
    ),
  ),
);
