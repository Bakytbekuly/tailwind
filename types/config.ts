export type ThemeConfigType = {
  app: {
    name: string;
  };
  layout: {
    darkMode: boolean;
    semiDarkMode: boolean;
    skin: SkinType;
    contentWidth: ContentWidthType;
    type: MenuType;
    navBarType: NavTypesValueType;
    footerType: FooterTypesValueType;
    isMonochrome: boolean;
    menu: {
      isCollapsed: boolean;
      isHidden: boolean;
    };
    mobileMenu: boolean;
    customizer: boolean;
    isRTL: boolean;
  };
};

//Menu
type MenuType = "horizontal" | "vertical";

//navbar
export type NavTypesType = {
  label: NavTypesLabelType;
  value: NavTypesValueType;
};

type NavTypesLabelType = "Sticky" | "Static" | "Floating" | "Hidden";
type NavTypesValueType = Lowercase<NavTypesLabelType>;

//skin
type SkinType = "bordered" | "default";

//

type ContentWidthType = "full" | "boxed";

//footer
export type FooterType = {
  label: FooterTypesLabelType;
  value: FooterTypesValueType;
};

type FooterTypesLabelType = "Sticky" | "Static" | "Hidden";
type FooterTypesValueType = Lowercase<FooterTypesLabelType>;
