import { ThemeConfigType } from "./config";

type SimplType = ThemeConfigType["layout"];

export type SettingsStoreType = {
  /*-----------------------------------------------------*/
  menuType: SimplType["type"];
  switchMenuType: (type: SimplType["type"]) => void;
  /*-----------------------------------------------------*/

  darkMode: SimplType["darkMode"];
  setDarkMode: (mode: SimplType["darkMode"]) => void;
  /*-----------------------------------------------------*/

  isCollapsed: SimplType["menu"]["isCollapsed"];
  setIsCollapsed: (isCollapsed: SimplType["menu"]["isCollapsed"]) => void;
  /*-----------------------------------------------------*/

  navBarType: SimplType["navBarType"];
  setNavBarType: (navBarType: SimplType["navBarType"]) => void;
  /*-----------------------------------------------------*/

  skin: SimplType["skin"];
  setSkin: (skin: SimplType["skin"]) => void;
  /*-----------------------------------------------------*/

  isRtl: SimplType["isRTL"];
  setIsRtl: (isRtl: SimplType["isRTL"]) => void;
  /*-----------------------------------------------------*/

  mobileMenu: SimplType["mobileMenu"];
  setMobileMenu: (mobileMenu: SimplType["mobileMenu"]) => void;
  /*-----------------------------------------------------*/

  customizer: SimplType["customizer"];
  setCustomizer: (customizer: SimplType["customizer"]) => void;
  /*-----------------------------------------------------*/

  semiDarkMode: SimplType["semiDarkMode"];
  setSemiDarkMode: (semiDarkMode: SimplType["semiDarkMode"]) => void;
  /*-----------------------------------------------------*/

  contentWidth: SimplType["contentWidth"];
  setContentWidth: (contentWidth: SimplType["contentWidth"]) => void;
  /*-----------------------------------------------------*/

  menuHidden: SimplType["menu"]["isHidden"];
  setMenuHidden: (menuHidden: SimplType["menu"]["isHidden"]) => void;
  /*-----------------------------------------------------*/

  isMonochrome: SimplType["isMonochrome"];
  setIsMonochrome: (isMonochrome: SimplType["isMonochrome"]) => void;
  /*-----------------------------------------------------*/

  footerType: SimplType["footerType"];
  setFooterType: (footerType: SimplType["footerType"]) => void;
  /*-----------------------------------------------------*/

  setResetConfigs: () => void;
};
