"use client";
import { useAuthStore } from "@/store";
import { redirect, useRouter } from "next/navigation";
import { useEffect } from "react";

const RequireAuth = (WrappedComponent: React.ComponentType) => {
  return function WithAuth(props: any) {
    const { user } = useAuthStore();
    useEffect(() => {
      if (!user) {
        // notFound()
        redirect("/login");
      }
    }, []);
    if (!user) {
      return null;
    }

    return <WrappedComponent {...props} />;
  };
};

export default RequireAuth;
