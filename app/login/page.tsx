import LoginPage from "@/components/partials/login/Login";
import React from "react";

const Login = () => {
  return (
    <>
      <LoginPage />
    </>
  );
};

export default Login;
