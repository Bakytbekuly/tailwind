"use client";

import { useLayoutStore } from "@/store";
import { Suspense } from "react";
import { usePathname } from "next/navigation";
import { ToastContainer } from "react-toastify";
import Header from "@/components/partials/header";
import Settings from "@/components/partials/settings";
import useWidth from "@/hooks/useWidth";
import MobileMenu from "@/components/partials/sidebar/MobileMenu";
import Loading from "@/components/ui/Loading";
import { motion } from "framer-motion";
import Sidebar from "@/components/partials/sidebar";
import Breadcrumbs from "@/components/ui/Breadcrumbs";
import MobileFooter from "@/components/partials/footer/MobileFooter";
import Footer from "@/components/partials/footer";
import RequireAuth from "@/HOC/RequireAuth";

function Home({ children }: any) {
  const {
    isRtl,
    darkMode,
    skin,
    isCollapsed,
    navBarType,
    menuType,
    menuHidden,
    mobileMenu,
    contentWidth,
    setMobileMenu,
  } = useLayoutStore();
  const { width, breakpoints } = useWidth();
  const location = usePathname();

  // header switch class
  const switchHeaderClass = () => {
    if (menuType === "horizontal" || menuHidden) {
      return "ltr:ml-0 rtl:mr-0";
    } else if (isCollapsed) {
      return "ltr:ml-[72px] rtl:mr-[72px]";
    } else {
      return "ltr:ml-[248px] rtl:mr-[248px]";
    }
  };

  return (
    <main
      dir={isRtl ? "rtl" : "ltr"}
      className={` app-warp ${darkMode ? "dark" : "light"} ${
        skin === "bordered" ? "skin--bordered" : "skin--default"
      } ${navBarType === "floating" ? "has-floating" : ""}`}
    >
      <ToastContainer />
      <Header
        className={width > Number(breakpoints.xl) ? switchHeaderClass() : ""}
      />
      {menuType === "vertical" &&
        width > Number(breakpoints.xl) &&
        !menuHidden && <Sidebar />}
      <MobileMenu
        className={`${
          width < Number(breakpoints.xl) && mobileMenu
            ? "left-0 visible opacity-100  z-[9999]"
            : "left-[-300px] invisible opacity-0  z-[-999] "
        }`}
      />
      {/* mobile menu overlay*/}
      {width < Number(breakpoints.xl) && mobileMenu && (
        <div
          className="overlay bg-slate-900/50 backdrop-filter backdrop-blur-sm opacity-100 fixed inset-0 z-[999]"
          onClick={() => setMobileMenu(false)}
        ></div>
      )}
      <Settings />

      <div
        className={`content-wrapper transition-all duration-150 ${
          width > 1280 ? switchHeaderClass() : ""
        }`}
      >
        {/* md:min-h-screen will h-full*/}
        <div className="page-content   page-min-height  ">
          <div
            className={
              contentWidth === "boxed" ? "container mx-auto" : "container-fluid"
            }
          >
            <motion.div
              key={location}
              initial="pageInitial"
              animate="pageAnimate"
              exit="pageExit"
              variants={{
                pageInitial: {
                  opacity: 0,
                  y: 50,
                },
                pageAnimate: {
                  opacity: 1,
                  y: 0,
                },
                pageExit: {
                  opacity: 0,
                  y: -50,
                },
              }}
              transition={{
                type: "tween",
                ease: "easeInOut",
                duration: 0.5,
              }}
            >
              <Suspense fallback={<Loading />}>
                <Breadcrumbs />
                {children}
              </Suspense>
            </motion.div>
          </div>
        </div>
      </div>
      {width < Number(breakpoints.md) && <MobileFooter />}
      {width > Number(breakpoints.md) && (
        <Footer
          className={width > Number(breakpoints.xl) ? switchHeaderClass() : ""}
        />
      )}
    </main>
  );
}

export default Home;
