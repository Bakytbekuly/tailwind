"use client";
import RequireAuth from "@/HOC/RequireAuth";
import React from "react";

function About() {
  return <div>About</div>;
}

export default RequireAuth(About);
