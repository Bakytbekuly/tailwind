import { ThemeConfigType } from "@/types/config";

export const NAME_TEXT = {
  name: "LayoutStore",
};

const themeConfig: ThemeConfigType = {
  app: {
    name: "Tailwind Next",
  },
  // layout
  layout: {
    darkMode: false,
    isRTL: false,
    skin: "default",
    contentWidth: "full",
    type: "vertical",
    navBarType: "sticky",
    footerType: "static",
    isMonochrome: false,
    menu: {
      isCollapsed: false,
      isHidden: false,
    },
    mobileMenu: false,
    customizer: false,
    semiDarkMode: false,
  },
};

export default themeConfig;
